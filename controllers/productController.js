const { isValidObjectId } = require("mongoose");
const Product = require("../models/product");
const User = require("../models/user");

const getProducts = (req, res) => {
    Product.find({ postId: req.params.postId }, (err, products) => {
        res.status(200).send(products.map(({ _id: productId, postId, title, imgUrl, productUrl, productDetails, description, productFeatures, productRating,  pros, cons }) => ({ productId, postId, title, imgUrl, productUrl, productDetails, productRating, description, productFeatures, pros, cons })))
        // var newProducts = [];
        // products.map(({ _doc: product }, i) => {
        //     !err && User.findById(product.addedById, (err, addedBy) => {
        //         product.find({
        //                 _id: { $ne: product._id },
        //                 categories: { $elemMatch: { $in: product.categories } }
        //             })
        //             .then((relatedproducts) => {
        //                 relatedproducts = relatedproducts.map(({ _doc: { title, dateAdded, categories } }) => ({ title, dateAdded, categories }))
        //                 const { addedById, ...productData } = product;
        //                 newProducts.push({...productData, addedBy, relatedproducts });
        //             })
        //             .finally(() => {
        //                 i == products.length - 1 &&
        //                     res.status(200).send(newProducts)
        //             })
        //     });
        // })
    })
}


const createProduct = (req, res) => {
    let newProduct = new Product({
        postId: req.params.postId,
        title: req.body.title || '',
        imgUrl: req.body.imgUrl || '',
        productUrl: req.body.productUrl || '',
        productDetails: req.body.productDetails || '',
        productFeatures: req.body.productFeatures || '',
        productRating: req.body.productRating || '',
        description: req.body.description || '',
        pros: req.body.pros || '',
        cons: req.body.cons || ''
    });
    // productFeatures: req.body.productFeatures.split('\n') || [],
    //     description: req.body.description || '',
    //     pros: req.body.pros.split('\n') || [],
    //     cons: req.body.cons.split('\n') || []
    newProduct.save((err, product) => {
        !err ? res.status(201).send(product) : res.status(404).send({ error: err })
    });
};

const updateProduct = (req, res) => {
    Product.findOneAndUpdate({_id: req.params.productId, postId: req.params.postId}, {
        title: req.body.title || '',
        imgUrl: req.body.imgUrl || '',
        productUrl: req.body.productUrl || '',
        productDetails: req.body.productDetails || '',
        productFeatures: req.body.productFeatures || '',
        productRating: req.body.productRating || '',
        description: req.body.description || '',
        pros: req.body.pros || '',
        cons: req.body.cons || ''
    }, { new: true })
        .then(({ _id: productId, postId, title, imgUrl, productUrl, productDetails, description, productFeatures, productRating, pros, cons }) => res.status(200).send({ productId, postId, title, imgUrl, productUrl, productRating, productDetails, description, productFeatures, pros, cons }))
        .catch((err) => res.status(404).send({ error: err }))
}

const getProductById = (req, res) => {
    isValidObjectId(req.params.productId) &&
        Product.findOne({_id: req.params.productId, postId: req.params.postId}, (err, product) => {
            !err && product ? res.status(200).send(product) :
            res.status(404).send({ error: `Product with ID ${req.params.productId} does not exist!` })
        })
    }

const deleteProduct = (req, res) => {
    isValidObjectId(req.params.productId) ?
        Product.findByIdAndDelete(req.params.productId, (err, product) => {
            if (!err && product) {
                const { _id: productId, title, description, categories, dateAdded, productsTitle, coverImg } = product;
                res.status(200).send({ productId, title, description, categories, dateAdded, productsTitle, coverImg })
            } else { res.status(404).send({ error: err }) }
        }) :
        res.status(404).send({ error: `Product with ID ${req.params.productId} does not exist!` })
}





module.exports = { getProducts, createProduct, getProductById, deleteProduct, updateProduct }