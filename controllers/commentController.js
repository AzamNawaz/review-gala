const Comment = require('../models/comment');

const getComments = (req, res) => {
    Comment.find().then((comments) => {
        res.status(200).send(comments)
    })
        .catch(err => res.status(400).send({ error: err }))
};

const getRecentComments = (req, res) => {
    Comment.find({isApproved: true}).limit(5).sort({ datePosted: 1 })
        .then((recentComments) => {
            res.status(200).send(recentComments)
        })
        .catch((err) => {
            res.status(400).send({ error: err })
        })
};

const updateComment = (req, res) => {
    Comment.findByIdAndUpdate(req.params.commentId, {
        isApproved: req.body.isApproved
    }, {new: true})
        .then((comment) => {
            res.status(201).send(comment)
        })
        .catch((err) => {
            res.status(400).send({ error: err })
        })
}

const deleteComment = (req, res) => {
    Comment.findByIdAndDelete(req.params.commentId)
        .then((comment) => {
            res.status(200).send(comment)
        })
        .catch((err) => {
            res.status(400).send({ error: err })
        })
}

const postComment = (req, res) => {
    const { name, email, message } = req.body;
    const { postId } = req.params;
    let newComment = new Comment({
        name,
        email,
        message,
        postId,
        isApproved: false,
        datePosted: new Date()
    });
    newComment.save()
        .then((comment) => {
            res.status(201).send(comment);
        })
        .catch((err) => {
            res.status(400).send({ error: err })
        })
}

module.exports = { updateComment, postComment, getComments, getRecentComments, deleteComment }