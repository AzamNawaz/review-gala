const { isValidObjectId } = require("mongoose");
const Post = require("../models/post");
const Product = require("../models/product");
const User = require("../models/user");
const Comment = require('../models/comment');

const getPosts = (req, res) => {
    if (!req.query.s) {
        Post.find((err, posts) => {
            res.status(200).send(posts.map(({ _id: postId, title, description, categories, dateAdded, productsTitle, coverImg }) => ({ postId, title, description, categories, dateAdded, productsTitle, coverImg })))
        })
    } else {
        Post.find({ $text: { $search: req.query.s }},(err, posts) => {
            res.status(200).send(posts.map(({ _id: postId, title, description, categories, dateAdded, productsTitle, coverImg }) => ({ postId, title, description, categories, dateAdded, productsTitle, coverImg })))
        })
    }
    // var newPosts = [];
    // posts.map(({ _doc: post }, i) => {
    //     !err && User.findById(post.addedById, (err, addedBy) => {
    //         Post.find({
    //                 _id: { $ne: post._id },
    //                 categories: { $elemMatch: { $in: post.categories } }
    //             })
    //             .then((relatedPosts) => {
    //                 relatedPosts = relatedPosts.map(({ _doc: { title, dateAdded, categories } }) => ({ title, dateAdded, categories }))
    //                 const { addedById, ...postData } = post;
    //                 newPosts.push({...postData, addedBy, relatedPosts });
    //             })
    //             .finally(() => {
    //                 i == posts.length - 1 &&
    //                     res.status(200).send(newPosts)
    //             })
    //     });
    // })
}


const createPost = (req, res) => {
    let newPost = new Post({
        title: req.body.title || '',
        description: req.body.description || '',
        categories: req.body.categories || [],
        coverImg: req.body.coverImg || '',
        dateAdded: new Date(),
        addedById: req.body.addedById || '',
        productsTitle: req.body.productsTitle || '',
        products: req.body.products || [],
        conclusion: req.body.conclusion || '',
        faqs: req.body.faqs || [],
    });
    newPost.save((err, post) => {
        !err ? res.status(201).send(post) : res.status(404).send({ error: err })
    });
};

const updatePost = (req, res) => {
    Post.findByIdAndUpdate(req.params.postId, {
        title: req.body.title || '',
        description: req.body.description || '',
        categories: req.body.categories || [],
        coverImg: req.body.coverImg || '',
        productsTitle: req.body.productsTitle || '',
    }, { new: true })
        .then(({ _id: postId, title, description, categories, dateAdded, productsTitle, coverImg }) => res.status(200).send({ postId, title, description, categories, dateAdded, productsTitle, coverImg }))
        .catch((err) => res.status(404).send({ error: err }))
}

const getPostById = (req, res) => {
    isValidObjectId(req.params.postId) ?
        Post.findById(req.params.postId, (err, post) => {
            !err && User.findById(post.addedById, (err, addedBy) => {
                Post.find({
                    _id: { $ne: post._id },
                    categories: { $elemMatch: { $in: post.categories } }
                }, (err, relatedPosts) => {
                    relatedPosts = relatedPosts.map(({ _doc: { title, dateAdded, categories } }) => ({ title, dateAdded, categories }))
                    const { addedById, ...postData } = post._doc;
                    Product.find({ postId: req.params.postId }).then((products) => {
                        Comment.find({postId: req.params.postId, isApproved: true}).then((comments) => {
                            res.status(200).send({ ...postData, addedBy, relatedPosts, products, comments });
                        })
                    })
                })
            })
        }) :
        res.status(404).send({ error: `Post with ID ${req.params.postId} does not exist!` })
}

const deletePost = (req, res) => {
    isValidObjectId(req.params.postId) ?
        Post.findByIdAndDelete(req.params.postId, (err, post) => {
            if (!err && post) {
                const { _id: postId, title, description, categories, dateAdded, productsTitle, coverImg } = post;
                res.status(200).send({ postId, title, description, categories, dateAdded, productsTitle, coverImg })
            } else { res.status(404).send({ error: err }) }
        }) :
        res.status(404).send({ error: `Post with ID ${req.params.postId} does not exist!` })
}


// const getPostsByDate = (req, res) => {
//     const reqDate = new Date(parseInt(req.params.year), parseInt(req.params.month));
//     Post.find({
//         $and: [
//             { $expr: { $eq: [{ $month: '$dateAdded' }, { $month: reqDate }] } },
//             { $expr: { $eq: [{ $year: '$dateAdded' }, { $year: reqDate }] } }
//         ]
//     }, (err, posts) => {
//         !err ? res.status(200).send(posts.map(({ title, description, categories }) => ({ title, description, categories }))) :
//             res.send(500).send({ error: err })
//     })
// }



module.exports = { getPosts, createPost, getPostById, deletePost, updatePost }