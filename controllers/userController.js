const { isValidObjectId } = require('mongoose');
const User = require('../models/user');
const nodemailer=require('nodemailer')

const getUsers = (req, res) => {
    User.find((err, users) => {
        res.send(users)
    })
};

const getUserById = (req, res) => {
    if (isValidObjectId(req.body.userId)) {
        User.findById(req.body.userId, (err, user) => {
            if (!err) { res.send({ user }) }
        })
    }
};

const signIn = (req, res) => {
    const { email, password } = req.body;
    try {
        User.findOne({
            email: email,
            password: password
        }, (err, user) => {
            if (!err && user) { res.status(200).send({email: user.email, name: user.name, userId: user._id, isAdmin: user.isAdmin}) }
            else { res.status(404).send({ error: 'User not found' });}
        })
    } catch (err) {
        res.status(500).send({ error: 'Something went wrong' });
    }
};

const signUp = (req, res) => {
    const { name, email, password } = req.body;
    try {
        let newUser = new User({
            name,
            email,
            password,
            isAdmin: false
        });
        newUser.save((err, user) => {
            if (!err) { res.status(201).send({email: user.email, name: user.name, userId: user._id, isAdmin: user.isAdmin}) }
        });
    } catch (err) {
        res.status(500).send({ error: 'Something went wrong' });
    }
};

const postMail = (req, res) => {
    const { name, email } = req.body;
    let transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        requireTLS: true,
        
        auth: {
            user: 'cd685be11@gmail.com',
            pass: '03234941542'
        }
    });
    var mailOptions = {
        from: "cd685be11@gmail.com",
        to: email,
        subject: `Welcome to ReviewsGala, ${name}!`,
        text: "Thanks, for contacting us!. We'll reply you soon.",

    }
    transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
            res.status(400).send({error});
            console.log(error)

        } else {
            res.status(200).send({message: "Thanks for contacting."})
        }
    })
}

module.exports = { getUsers, getUserById, signIn, signUp, postMail }