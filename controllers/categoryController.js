const { response, request } = require("express");
const res = require("express/lib/response");
const { isValidObjectId } = require("mongoose");
const Category = require("../models/category");
const Post = require("../models/category");
const User = require("../models/user");

const getcategories = (req, res) => {
    Category.find((err, category) => {
        res.send(category)
    })
}


const getcategoryById = (req, res) => {
    if (isValidObjectId(req.params.categoryId)) {
        Category.findById(req.params.categoryId, (err, category) => {
            if (!err) { res.send(category) } else { res.send({ message: 'Masti kr raha hai' }) }
        })
    } else {
        res.send({ message: `Category with ID ${req.params.categoryId} does not exist.` })

    }
};

const addCategory = (req, res) => {
    let newCategory = new Category({
        name: req.body.name,
    });
    newCategory.save((err, category) => {
        if (!err) { res.send({ message: 'Category added successfully' }) }
    });
};



module.exports = { getcategories, getcategoryById, addCategory }