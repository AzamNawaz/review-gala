const express = require('express');
const { getUsers, getUserById, signUp, postMail, signIn } = require('../controllers/userController');
const userRouter = express.Router();

userRouter.get('', getUsers);
userRouter.get('/id', getUserById);
userRouter.post('/signup', signUp)
userRouter.post('/signin', signIn);
userRouter.post('/send-mail',postMail)
module.exports = userRouter;