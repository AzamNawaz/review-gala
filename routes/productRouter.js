const express = require('express');
const { getProductById, deleteProduct, createProduct, getProducts, updateProduct } = require('../controllers/productController');
const productRouter = express.Router();

productRouter.get('/:postId', getProducts);
productRouter.post('/:postId', createProduct);
productRouter.delete('/:postId/:productId', deleteProduct)
productRouter.get('/:postId/:productId', getProductById);
productRouter.put('/:postId/:productId', updateProduct);



module.exports = productRouter;