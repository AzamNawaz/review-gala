const express = require('express');
const { getcategories, getcategoryById, addCategory } = require('../controllers/categoryController');
const CategoryRouter = express.Router();

CategoryRouter.get('', getcategories);
CategoryRouter.get('/:categoryId', getcategoryById);
CategoryRouter.post('/', addCategory);



module.exports = CategoryRouter;