const express = require('express');
const { getPosts, createPost, getPostById, deletePost, updatePost } = require('../controllers/postController');
const postRouter = express.Router();

postRouter.get('', getPosts);
postRouter.post('', createPost);
postRouter.delete('/:postId', deletePost)
postRouter.get('/:postId', getPostById);
postRouter.put('/:postId', updatePost);
// postRouter.get('/:year/:month', getPostsByDate);



module.exports = postRouter;