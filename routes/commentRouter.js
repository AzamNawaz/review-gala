const express = require('express');
const { postComment, getComments, updateComment, getRecentComments, deleteComment } = require('../controllers/commentController');
const CommentRouter = express.Router();

CommentRouter.get('', getComments);
CommentRouter.get('/recent', getRecentComments);
CommentRouter.post('/:postId', postComment);
CommentRouter.put('/:commentId', updateComment);
CommentRouter.delete('/:commentId', deleteComment)

module.exports = CommentRouter;