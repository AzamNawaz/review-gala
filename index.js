const express = require('express');
const bodyparser = require('body-parser');
const cors = require('cors');
const mongoose = require('./db.js');
const userRouter = require('./routes/userRouter.js');
const postRouter = require('./routes/postRouter.js');
const CategoryRouter = require('./routes/categoryRouter.js');
const productRouter = require('./routes/productRouter.js');
const CommentRouter = require('./routes/commentRouter.js');

const app = express();
const Port = process.env.PORT || 5000;

app.use(bodyparser.json({ limit: '5mb' }));
app.use(cors({ origin: '*' }));

app.listen(Port, () => console.log('Server started at port: ' + Port));

app.use('/users', userRouter)
app.use('/posts', postRouter)
app.use('/categories', CategoryRouter)
app.use('/products', productRouter)
app.use('/comments', CommentRouter)
