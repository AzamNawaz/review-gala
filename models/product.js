const { model } = require('mongoose');

const Product = model('Product', {
    postId: {type: String},
    title: { type: String },
    imgUrl: { type: String },
    productUrl: { type: String },
    description: { type: String },
    productDetails: { type: String },
    productFeatures: {type: String},
    productRating: {type: String},
    pros: {type: String},
    cons: {type: String}
});

module.exports = Product;