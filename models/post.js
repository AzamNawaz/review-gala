const { Schema, model } = require('mongoose');

const FAQSchema = Schema({
    question: { type: String },
    answer: { type: String }
});

const Post = model('Post', {
    title: { type: String, index: "text" },
    description: { type: String, index: "text" },
    categories: [{ type: String }],
    coverImg: { type: String },
    dateAdded: { type: Date },
    addedById: { type: String },
    productsTitle: { type: String },
    products: [Schema.Types.ObjectId, 'Product'],
    conclusion: { type: String },
    faqs: [FAQSchema]
});

module.exports = Post;