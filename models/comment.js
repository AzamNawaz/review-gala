const { model } = require('mongoose');

const Comment = model('Comment', {
    name: { type: String },
    email: { type: String },
    message: { type: String },
    postId: { type: String },
    isApproved: { type: Boolean },
    datePosted: { type: Date }
});

module.exports = Comment;