const mongoose = require('mongoose');

const User = mongoose.model('User', {
    name: { type: String },
    email: { type: String },
    isAdmin: { type: Boolean },
    password: { type: String }
});
module.exports = User